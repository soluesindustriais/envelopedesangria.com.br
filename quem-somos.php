<?php
$h1 = "Quem Somos";
$title = $h1;
$desc = "O Portal Soluções Industriais tem o objetivo de facilitar a busca de produtos e serviços em um único portal. Com ele você tem acesso a diversas ferramentas e todo o controle para uma pesquisa rápida e eficiente...";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php include('inc/head.php'); ?>
</head>

<body>
    <?php include 'inc/header.php' ?>
	<section class="second-bloc py-5" style="margin-top:-3rem">
		<div class="container">

            

                
				<div class="row">
					
					<div class="col-md-12 mb-4">
                     <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                        <h2 class="text-sec-block m-0 mb-4 text-center">Qualidade e comprometimento</h2>
                        <p class="text-justify">
                            O Soluções Industriais tem o objetivo de facilitar todas as suas buscas de produtos e serviços em um único portal. Com ele você tem acesso a diversas ferramentas e todo o controle para uma pesquisa ágil e eficiente. Você pode fazer solicitações de orçamentos, comparar produtos e entrar em contato diretamente com o fornecedor, tudo isso e muito mais em uma única plataforma. Interações de forma rápida e dinâmica com o consumidor final, acelerando a sua venda pela internet, além da plataforma estar presente em outros canais digitais, inovando o mercado industrial.
					   </p>
                        </div>
                    </div>
					
            </div>
							
				<div class="row">
                    
					<div class="col-md-4 mb-3">
                        <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                        <h2 class="text-center">MISSÃO</h2>
						<p class="font-quem-somos text-center">
							A missão atualmente do Portal Soluções Industriais é tornar a sua busca por um produto ou serviço industrial mais fácil. Inovação e eficiência no mercado industrial com uma plataforma que permite uma pesquisa personalizada e materiais de qualidade. Assim como a indústria, o Soluções Industriais possui uma tecnologia de ponta, pronta para facilitar a sua vida.
						</p>
                        </div>
					</div>
                    
					<div class="col-md-4 mb-3">
                        <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                        <h2 class="text-center">VISÃO</h2>
						<p class="font-quem-somos text-center">
                            O Soluções Industriais acredita que a tecnologia e a mão de obra humana podem entrar em parceria, por isso o Portal é considerado uma vitrine interativa para indústrias. A plataforma é uma opção excelente para divulgar produtos e serviços industriais com eficiência, trazendo um grande número de compradores e representantes comerciais, em diversas localidades no país e com muita visibilidade no exterior.</p>
                        </div>
                    </div>
                    
						<div class="col-md-4 mb-3" > 
                            <div class="quem-somos-bloco-3 w-100 p-0 p-4 h-100">
                            <h2 class="text-center text-center">VALORES</h2>							
                                <ul>
                                    <li>Fabricado com matérias-primas</li>
                                    <li>Polietileno</li>
                                    <li>Propileno;</li>
                                    <li>Coextrusado;</li>
                                    <li>Tecnologia avançada.</li>
                                    <li>Presente em canais digitais</li>
                                </ul>
                            
                            </div>
						</div>
					</div>
					
						
				<p class="w-100 text-center">
				A tecnologia Embalagem Ideal® é uma patente da empresa Soluções Industriais®, integrante do Grupo Ideal Trends®
				</p>
						
				
        </div>
				</section>
			

    <?php include 'inc/footer.php' ?>
</body>

</html>
