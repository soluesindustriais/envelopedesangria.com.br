<section>
    <div class="container-fluid">
        <div class="row">
          
            <div class="col-12 background-bloco py-5 p-md-5 align-items-center justify-content-center">
                <div class="col-12 col-md-8">               
                    <h2 class="text-destaq text-center">Informações</h2>
                    <p class="sec-p text-dark text-center">Os envelopes atualmente estão sendo muito utilizados em várias áreas da indústria por sua composição feita com polipropileno, oferecendo segurança e qualidade aos seus produtos. Eles garantem melhor armazenamento e podem transportar objetos com mais segurança, garantindo que os produtos cheguem ao seu destino intactos e sem que ocorram problemas durante o trajeto.</p>



                    <div class="row d-flex justify-content-center">

                        <div class="col-md-4 col-sm-12" style="padding:15px">
                            <div class="quadro-depo m-0 d-flex w-100 align-items-center justify-content-center">

                                <img src="<?=$url?>assets/img/icons/bag.svg" width="100" alt="Segurança">
                                <p class="mt-3 mb-0">Qualidade</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12" style="padding:15px">
                            <div class="quadro-depo m-0 d-flex w-100 align-items-center justify-content-center">

                                <img src="<?=$url?>assets/img/icons/pet-food.svg" width="100" alt="Impermeabilidade">
                                <p class="mt-3 mb-0">Impermeabilidade</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12" style="padding:15px">
                            <div class="quadro-depo m-0 d-flex w-100 align-items-center justify-content-center">

                                <img src="<?=$url?>assets/img/icons/shopping-bag.svg" width="100" alt="Resistência">
                                <p class="mt-3 mb-0">Flexibilidade</p>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center mt-2 mb-4">
                            <a href="<?=$url?>informacoes" class="button-slider2">Saiba Mais</a>
                        </div>
                    </div>
                </div>
            </div>




        </div>

    </div>
</section>
