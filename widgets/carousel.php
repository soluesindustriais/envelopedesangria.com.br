<div id="carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators lineSlide">
		<li data-target="#carousel" data-slide-to="0" class="active"></li>
		<li data-target="#carousel" data-slide-to="1"></li>
		<li data-target="#carousel" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner">
		<div class="carousel-item active one">
			<div class="d-flex justify-content-center align-items-center scont">
                <div class="col-md-9 col-11 text-center" style="margin-top:-50px">
				<h2 class="stitulo" style="">Envelopes</h2>
				<p class="sdesc" style="">Armazenamento de qualidade para os seus produtos.</p>
				<a href="<?=$url?>envelope-a3" class="btn button-slider2">Saiba Mais</a>
                </div>
			</div>
		</div>	
		<div class="carousel-item three">
			<div class="d-flex justify-content-center align-items-center scont">
                <div class="col-md-9 col-11 text-center" style="margin-top:-50px">
				<h2 class="stitulo"  style="">Envelopes de papel</h2>
				<p class="sdesc" style="">Segurança e resistência para o transporte dos seus documentos.</p>
				<a href="<?=$url?>envelope-de-papel" class="btn button-slider2">Saiba Mais</a>
			</div>
            </div>
		</div>
        <div class="carousel-item two">
			<div class="d-flex justify-content-center align-items-center scont">
                <div class="col-md-9 col-11 text-center" style="margin-top:-50px">
				<h2 class="stitulo"  style="">Envelope plástico</h2>
				<p class="sdesc"  style="">Garanta organização e praticidade para os seus produtos</p>
				<a href="<?=$url?>envelope-plastico-4-furos" class="btn button-slider2">Saiba Mais</a>
			</div>
            </div>
		</div>
	</div>

	<a class="carousel-control-prev " href="#carousel" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon seta" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>

	<a class="carousel-control-next " href="#carousel" role="button" data-slide="next">
		<span class="carousel-control-next-icon seta" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>

</div>

