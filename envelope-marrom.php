<?php
include('inc/vetKey.php');
$h1 = "envelope marrom";
$title = $h1;
$desc = "Envelope marrom Muitas pessoas conhecem por papel ou envelope pardo, mas a verdade é que aquele papel pardo tão utilizado no dia a dia das pessoas é";
$key = "envelope,marrom";
$legendaImagem = "Foto ilustrativa de envelope marrom";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
 
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include('inc/head.php'); ?>
    <link rel="stylesheet" href="<?=$url?>assets/css/mpi-style.css">
</head>

<body>
    <?php include 'inc/header.php' ?>   
        <div class="container">
            <div class="row">
                <article class="col-md-9 col-12">
                    <?php $quantia = 3;
                    include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Envelope marrom</h2><p>Muitas pessoas conhecem por papel ou envelope pardo, mas a verdade é que aquele papel pardo tão utilizado no dia a dia das pessoas é chamado de envelope de papel kraft. A popularização do conhecimento do termo kraft é muito recente, mas o uso desse tipo de papel já comum há muito tempo. Isso porque suas propriedades são ideais para diversas finalidades, o que aumenta a busca por um produto tão versátil e de baixo custo. </p><h2>O que é o envelope marrom</h2><p>O envelope marrom, ou envelope de papel kraft é uma mistura de muitos tipos fibras de celulose. A celulose é encontrada nas paredes celulares de plantas, e no caso do tipo de fibra que leva até o envelope marrom, essas fibras normalmente são retiradas de polpas de alguns tipos de madeiras mais conhecidos por sua maciez.</p><p>Devido a essa origem, a fibra que compõe o papel marrom é tão resistente e flexível ao mesmo tempo. Uma vez extraída a fibra da madeira ela é trabalhada para poder assumir a forma do produto final desejado, mas nesse processo não está inclusa a fase de branqueamento, o que permite ao papel kraft se tornar um envelope marrom como se vê nas lojas.</p><h2>Utilidades do envelope marrom</h2><p>Como as pessoas sabem de sua resistência claramente mais alta que os envelopes de papel comum, é muito comum que as pessoas utilizem o envelope marrom em diversas situações. Tais como:</p><ul><li>Armazenamento de documentos;</li><li>Armazenamento de arquivos empresariais;</li><li>Armazenamento de itens como diplomas;</li><li>Transporte e envio de dinheiro.</li></ul><p>Todos esses usos são comuns já que o envelope comum irá oferecer aos conteúdos maior proteção durante o período de armazenamento. Além disso, o fechamento do envelope marrom pode ser realizado facilmente com fitas.</p><p>É importante ainda destacar que devido a seu baixo custo, é possível substituir facilmente o envelope marrom quando desejado. Além disso sua cor e textura permitem que escritas sejam realizadas com facilidade no papel kraft, o que possibilita a inserção de marcações que auxiliem na procura de algum conteúdo específico, ainda mais quando se pensa no uso do envelope marrom em arquivos empresariais, por exemplo.</p><!--EndFragment-->

                    </article>
                <?php include('inc/coluna-lateral.php'); ?>
                <br class="clear" />
                <?php include('inc/paginas-relacionadas.php'); ?>
                <?php include('inc/regioes.php'); ?>
                <br class="clear">
                <?php include('inc/copyright.php'); ?>
            </div>
        </div>    
    <?php include('inc/footer.php'); ?>
</body>
</html>