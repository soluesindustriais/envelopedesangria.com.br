<?php
$prodRelatedItems = explode("-", $prod_name);
$prodRelatedQuery = "";
$prodRelatedFilter = ['da', 'de', 'do', 'e', 'em', 'para', 'sob', 'tipo']; //Lista de palavras genéricas para desconsiderar na pesquisa (Use apenas letras minúsculas e sem acentuação)
$prodRelatedItems = array_diff($prodRelatedItems, $prodRelatedFilter);

foreach($prodRelatedItems as $key => $prodWord) {
  $prodRelatedQuery .= "prod_name LIKE '%' '".$prodWord."' '%'";
  if ($prodWord !== end($prodRelatedItems)) {
    $prodRelatedQuery .= " OR ";
  }
}

$Read->ExeRead(TB_PRODUTO, "WHERE prod_status = :st AND prod_id != :id AND user_empresa = :emp AND (".$prodRelatedQuery.") ORDER BY RAND() LIMIT 0,4", "st=2&id={$prod_id}&emp=" . EMPRESA_CLIENTE);

if ($Read->getResult()): ?>
  <div class="container">
    <div class="prod-inc__related">
      <h2>Páginas relacionadas</h2>
      <div class="row">
        <?php
        foreach ($Read->getResult() as $prod_related):
          extract($prod_related); ?>
          <div class="p-2 col-3">
            <div class="card card--prod">
              <a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $prod_name; ?>" title="<?= $prod_title; ?>">
                <img class="card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$prod_cover?>" alt="<?=$prod_title?>" title="<?=$prod_title?>">
                <h2 class="card__title"><?= $prod_title; ?></h2>
                <!-- SE ESTIVER SETADO PARA TRUE NA CLIENT.INC.PHP MOSTRA DESCRIÇÃO NA THUMB DO PRODUTO  -->
                <?php if (PROD_BREVEDESC): ?>
                  <div class="card__description">
                    <?= $prod_brevedescription?>
                  </div>
                <?php endif; ?>
              </a>
            </div>
          </div>
        <? endforeach; ?>
      </div>
    </div>
  </div>
<? endif; ?>